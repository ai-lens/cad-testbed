#Authored by Ben Filbert, Dec 2018
import os
import json



class JsonConverter(object):
    def __init__(self,fname):
        self.fname = fname
        with open(self.fname) as f:
                file_n = f.read()
                self.converted = json.loads(file_n)

        


    def ConvertedValues(self):
        #print("!")
        return self.converted["groups"][0]["lenses"]

    def NeckSizeRaw(self):
        last = self.converted["groups"][0]["lenses"][0][-1]
        first = self.converted["groups"][0]["lenses"][0][0]
        y = first[1]
        #max(first[1],last[1])
        b = self.converted["sensorToBack"]
        #print( ( b + 6.8))
        Y = (y - ( b + 6.8))
        return Y
    

    def ConvertedFile(self,values):
        fname = open("/Users/ben/Desktop/CourseWork/SeniorWinter/Senior Design/PySolutionTest/SrcSamples/IntermediateValues.txt","w+")
        
        points = values[0]
        #print(len(points))
        i = 0
        y = 0
        for l in points:
            if (i == 0):
                y = l[1]

            fname.write(  str(l[0])+" "+str(l[1]-y)+"\n")
            i = i + 1
        fname.close()


