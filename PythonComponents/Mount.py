#Authored by Ben Filbert, Dec 2018
from dxfwrite import DXFEngine as dxf
import os

class Mount(object):
    def __init__(self, lip,drawing):
        self.lip = lip # inner Start
        self.drawing = drawing
        self.outerStart = [-28,2] # away from innerEnd by 0.1   # height between 1 and 0
        self.outerEnd = [-32.5,2] # .9 from outerStart
        self.innerEnd = [-27.5,2] # Max size of lens

    def Save(self):
        self.drawing.save()

    def Draw(self,maxDip):
       
        pointList = []
        for p in self.lip:
            
            pointList.append(p)

        pointList.append([self.lip[0][0],2]) 
        pointList.append(self.innerEnd)
        pointList.append(self.outerStart)
        pointList.append(self.outerEnd)

        pointList.append([self.outerEnd[0],-1.6])
        pointList.append([self.outerStart[0],-1.6])
        pointList.append([self.innerEnd[0],-1.6])
        # pointList.append([self.lip[len(self.lip)-1][0]-.8,-1.6])
        # pointList.append([self.lip[len(self.lip)-1][0]-.4,maxDip])
        # pointList.append([self.lip[len(self.lip)-1][0]-.1,maxDip])
        pointList.append([self.lip[len(self.lip)-1][0],-1.6]) 

       

        #print(pointList)

        I = 0
        while I <=len(pointList)-1:

            line = dxf.line()
            if ( I == len(pointList)-1) :
                line['start'] = pointList[I]
                line['end'] = pointList[0]
            else:
                line['start'] = pointList[I]
                line['end'] = pointList[I+1]
            self.drawing.add(line)
            I+=1
        


