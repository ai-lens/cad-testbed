#Authored by Ben Filbert, Dec 2018
import os

class Parser(object):
    def __init__(self, fname):
        self.pointList = []
        self.fname = fname
        self.lip = []
        
    
    def PrintPointsList(self):
        print (self.pointList)

    def Parse(self):
        with open(self.fname) as f:
            for l in f:
                l.rstrip('\n')
                temp = l.split()
                try:
                    self.pointList.append((float(temp[0]), float(temp[1])))
                except:pass

    def AddLip(self):
        topPoint = [self.pointList[0][0], 0.6]
        botPoint = [self.pointList[  len(self.pointList)-1][0], 0]
        topOuter = [self.pointList[0][0] - 0.5 , 0.6]
        botOuter = [self.pointList[  len(self.pointList)-1][0] -0.5, 0]
        #print (self.pointList)
        self.pointList.insert(0,topPoint)
        self.pointList.insert(0,topOuter)
        self.pointList.append(botPoint)
        self.pointList.append(botOuter)

        self.lip.append(botPoint)
        self.lip.append(botOuter)
        self.lip.append(topOuter)
        self.lip.append(topPoint)


        #print (self.pointList)

        
        
        #print(topPoint)
        #print(botPoint)
        #print(topOuter)
        #print(botOuter)
        


    def GetPoints(self):
        return self.pointList

    def GetLip(self):
        return self.lip