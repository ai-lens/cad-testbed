#Authored by Ben Filbert, Dec 2018
from dxfwrite import DXFEngine as dxf
import os
import pprint
import json

from Parser import Parser
from Model import Model
from Mount import Mount
from Neck import Neck
from JsonConverter import JsonConverter

dir_path = os.path.dirname(os.path.realpath(__file__))
drawing = dxf.drawing(dir_path+'/DXF_RESULTS/Optamized_Test_Lens.dxf')

#fname  = "/Users/ben/Desktop/CourseWork/SeniorWinter/Senior Design/PySolutionTest/SrcSamples/False50mm.txt"
# TO-DO : Change to get arguement from comm line
fname = "/Users/ben/Desktop/CourseWork/SeniorWinter/Senior Design/PySolutionTest/SrcSamples/a12-26.json"

JsonRaw = JsonConverter(fname)

file_n = JsonRaw.ConvertedValues()

JsonRaw.ConvertedFile(file_n)

dataPoints = "/Users/ben/Desktop/CourseWork/SeniorWinter/Senior Design/PySolutionTest/SrcSamples/IntermediateValues.txt"

parser = Parser(dataPoints)
model = Model(drawing)
parser.Parse()
#
WholeModel = Model(dxf.drawing(dir_path+'/DXF_RESULTS/Optamized_WholeModel.dxf'))
WholeModel.WholeDraw(parser.GetPoints())
WholeModel.Save()
#
model.FindMaxDip(parser.GetPoints())
model.Draw(parser.GetPoints())
#parser.AddLip()
#model.WholeDraw(parser.GetPoints())
model.Save()
#

parser.AddLip()

#
lip = Model(dxf.drawing(dir_path+'/DXF_RESULTS/Optamized_Test_Lip.dxf'))
lip.WholeDraw(parser.GetLip())
lip.Save()

mount = Mount(parser.GetLip(),dxf.drawing(dir_path+'/DXF_RESULTS/Mount_Optamized.dxf'))

mount.Draw(model.maxDip)
mount.Save()

y = JsonRaw.NeckSizeRaw()
#print(y)

neck = Neck(y,y/2,2,dxf.drawing(dir_path+'/DXF_RESULTS/Optamized_Neck.dxf'))
neck.Draw()
neck.Save()