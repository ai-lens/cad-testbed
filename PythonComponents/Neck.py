#Authored by Ben Filbert, Dec 2018
from dxfwrite import DXFEngine as dxf
import os

class Neck(object):
    def __init__(self,height,cavH,cavL,drawing):
        self.UpperStart = [-28,0] # away from innerEnd by 0.1   # height between 1 and 0
        self.UpperEnd = [-32.5,0] # .9 from outerStart
        self.LowerEnd = [-32.5,-height]
        self.LowerStart = [-28,-height]
        self.hieght = height
        self.cH = cavH
        self.cL = cavL
        self.drawing = drawing

    def Draw(self):
        pointList = []
        pointList.append(self.UpperStart)
        pointList.append(self.UpperEnd)
        pointList.append(self.LowerEnd)
        pointList.append(self.LowerStart)
        cavity = []
        
        cavity.append([-30.25+(self.cL/2),-(self.hieght/2)+(self.cH/2)])
        cavity.append([-30.25-(self.cL/2),-(self.hieght/2)+(self.cH/2)])
        
        cavity.append([-30.25-(self.cL/2),-(self.hieght/2)-(self.cH/2)])
        cavity.append([-30.25+(self.cL/2),-(self.hieght/2)-(self.cH/2)])

        I = 0
        while I <=len(pointList)-1:

            line = dxf.line()
            if ( I == len(pointList)-1):
                line['start'] = pointList[I]
                line['end'] = pointList[0]
            else:
                line['start'] = pointList[I]
                line['end'] = pointList[I+1]
            self.drawing.add(line)
            I+=1
        
        I = 0
        while I <=len(cavity)-1:

            line = dxf.line()
            if ( I == len(cavity)-1):
                line['start'] = cavity[I]
                line['end'] = cavity[0]
            else:
                line['start'] = cavity[I]
                line['end'] = cavity[I+1]
            self.drawing.add(line)
            I+=1

    def Save(self):
        self.drawing.save()
