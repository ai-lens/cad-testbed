#Authored by Ben Filbert, Dec 2018
from dxfwrite import DXFEngine as dxf
import os


class Model (object):
    def __init__(self,drawing):
        self.drawing = drawing
        self.halfA = []
        self.halfB = []
        self.isEven = False
        self.midX = 0
        self.maxDip = 0

    def Draw(self,pointList):

        pointList = self.Split(pointList)
        
        #print(pointList)

        # apply offset of midX
        I = 0
        while I <=len(pointList)-1:
            # -self.midX/2
            pointList[I] = [pointList[I][0]   ,pointList[I][1] ]
            I+=1
        
        #print(pointList)

        I = 0
        while I <=len(pointList)-1:

            line = dxf.line()
            if ( I == len(pointList)-1):
                line['start'] = pointList[I]
                line['end'] = pointList[0]
            else:
                line['start'] = pointList[I]
                line['end'] = pointList[I+1]
            self.drawing.add(line)
            I+=1

    def Save(self):
        self.drawing.save()
    
    def FindMaxDip(self,pointList):
        tempDip = 0
        for I in pointList:
            tempDip = I[1]
            if ( tempDip <= self.maxDip):
                self.maxDip = tempDip


    

    def Split(self,pointList):
        pointList = remove_adjacent(pointList)
        prevpoint = []
        foundMid = False
        for I in pointList:
            if (I == pointList[0] ):
                prevpoint = pointList[0]
                self.halfA.append(pointList[0])
            elif (I[0] != prevpoint[0] and not foundMid):
                prevpoint = I
                self.halfA.append(I)
            elif (I[0] == prevpoint[0] ):
                self.midX = I[0]
                foundMid = True
                self.halfB.append(I)
            elif (I[0] != prevpoint[0] and foundMid):
                prevpoint = I
                self.halfB.append(I)
        #print(self.halfA)
        #print("_")
        #print(self.halfB)
        #print (self.midX)
        #print( median(self.halfA) )
        #print( median(self.halfB) )
        
        #print("_")

        
        
        #self.halfB.reverse()
        self.halfB.remove(self.halfB[0])
        #print ( self.halfB   )
        middleIndexA = int((len(self.halfA) - 1)/2)
        middleIndexB = int((len(self.halfA) - 1)/2)
        #print(str(middleIndexA) + " " + str(middleIndexB))

        targA = median(self.halfA)
        targB = median(self.halfB)

        if(targA[0] > 0 ):
            while middleIndexA <= 0 and targA[0] > 0 :
                middleIndexA -=1
                if (self.halfA[middleIndexA][0] <= 0 ):
                    targA = self.halfA[middleIndexA]
        if(targB[0] > 0 ):
            while middleIndexB <= len(self.halfB) and targB[0] > 0 :
                middleIndexB -=1
                if (self.halfA[middleIndexB][0] <= 0 ):
                    targB = self.halfB[middleIndexB]
            


        J = len(pointList ) - 1
        pointList.clear()
        I = 0


        #print(self.halfA)
        #print(self.halfB)
        #print(targA)
        #print(targB)
        while I <= J :
            if len(self.halfA) > 0 :
                pointList.append(self.halfA[I])
                if self.halfA[I] == targA :
                    self.halfA.clear()
                    pointList.append([0,targA[1]])
            I +=1
        I = 0
        

        if (len(self.halfB ) > 2 ):
            plist =[]
            self.halfB.reverse()
            while I <= J :
                if len(self.halfB) > 0 :
                    plist.append(self.halfB[I])
                    if self.halfB[I] == targB :
                        self.halfB.clear()
                        pointList.append([0,targB[1]])
                I +=1

            I = 0
            plist.reverse()
            T = len(plist) -1
            while I <= T :
                if(T) > 0 :
                    pointList.append(plist[I])
                I +=1
        
        else :
             while I <= J :
                if len(self.halfB) > 0 :
                    pointList.append(self.halfB[I])
                    if self.halfB[I] == targB :
                        self.halfB.clear()
                I +=1

        #print(pointList )
        return pointList
        
    def WholeDraw(self,pointList):
        I = 0
        while I <=len(pointList)-1:

            line = dxf.line()
            if ( I == len(pointList)-1):
                line['start'] = pointList[I]
                line['end'] = pointList[0]
            else:
                line['start'] = pointList[I]
                line['end'] = pointList[I+1]
            self.drawing.add(line)
            I+=1


def median(lst):
    #print("_")
    #print(lst)
    n = len(lst)
    if n < 1:
            return None
    if n == 2 :
        
        return lst[n-1]
    if n % 2 == 1:
            return sorted(lst)[int(n/2)]
    else:
            temp1 = lst[int((n/2)-1)]
            return temp1


def remove_adjacent(seq): 
    i = 1
    n = len(seq)
    for j in seq:
        if j == seq[i-1]:
            del seq[i]
            n -= 1
        else:
            i += 1
    return seq


            


        


    