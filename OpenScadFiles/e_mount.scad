include <nutsnbolts-master/cyl_head_bolt.scad>;

$fn = 1000;
module Mount(){ 

translate([0,0,-15])
rotate_extrude( angle = 360,convexity = 10)
import("C:/Users/Ben/Desktop/SENIORPROJECT/50mm/LensNeck_Test_50mm.dxf");

  
}
module union()
{
import("C:/Users/Ben/Desktop/SENIORPROJECT/e mount.stl");
Mount();
//translate([0,0,-19])
//Connector();
}
module Connector()
{
rotate_extrude( angle = 360,convexity = 10)
translate([29,0,-20])
    square([5,1]);
 }
difference(){
union();
translate([0, -30, -5]) hole_threaded(name = "M4", l=30,thread="yes",cltd = 0.0);
translate([0, 30, -5]) hole_threaded(name = "M4", l=30,thread="yes",cltd = 0.0);
    
translate([-30, 0, 3]) hole_threaded(name = "M4", l=30,thread="yes",cltd = 0.0);
translate([30, 0, 3]) hole_threaded(name = "M4", l=30,thread="yes",cltd = 0.0);
    
 rotate_extrude( angle = 360,convexity = 10)
translate([28.,-20,0])
square([.7,1.6]);

}
 
difference();

