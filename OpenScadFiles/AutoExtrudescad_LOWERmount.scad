include <nutsnbolts-master/cyl_head_bolt.scad>;


$fn = 500;
module Mount(){ 
rotate_extrude( angle = 360,convexity = 10)

 import("C:/Users/Ben/Desktop/SENIORPROJECT/DXF_RESULTS/Mount_Optamized.dxf");


    
}
module Difference(){

difference(){
Mount();
translate([0, -30, 3]) hole_threaded(name = "M4", l=15,thread="yes",cltd = 0.0);
translate([0, 30, 3]) hole_threaded(name = "M4", l=15,thread="yes",cltd = 0.0);

translate([-30, 0, 3]) hole_threaded(name = "M4", l=30,thread="yes",cltd = 0.0);
translate([30, 0, 3]) hole_threaded(name = "M4", l=30,thread="yes",cltd = 0.0);

rotate_extrude( angle = 360,convexity = 10)
translate([0,.5,0])
    square([50,100]);
    
    rotate_extrude( angle = 360,convexity = 10)
translate([28,-.5,0])
square([.7,1.6]);
}

}

module Union()
{
    
    union(){
Difference();
rotate_extrude( angle = 360,convexity = 10)
translate([28.1,-1.99,0])
square([.4,.5]);
    }

}
 rotate([0,180,0])
Union();


//