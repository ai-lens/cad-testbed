
 include <nutsnbolts-master/cyl_head_bolt.scad>;


$fn = 1000;
module Mount(){ 
rotate_extrude( angle = 360,convexity = 10)

import("C:/Users/Ben/Desktop/SENIORPROJECT/DXF_RESULTS/Optamized_Neck.dxf");



    
}
module Difference(){

difference()
   {
Mount();
translate([0, -30, 3]) hole_threaded(name = "M4", l=120,thread="yes",cltd = 0.0);
translate([0, 30, 3]) hole_threaded(name = "M4", l=120,thread="yes",cltd = 0.0);

translate([-30, 0, 3]) hole_threaded(name = "M4", l=120,thread="yes",cltd = 0.0);
translate([30, 0, 3]) hole_threaded(name = "M4", l=120,thread="yes",cltd = 0.0);

 rotate_extrude( angle = 360,convexity = 10)
translate([28.1,-.5,0])
square([.7,1.6]);
   }
   
}
 


 module Union()
{
    union()
    {
Difference();
rotate_extrude( angle = 360,convexity = 10)
translate([28.1,-5.4,0])
square([.4,.5]);
    }

}
rotate([0,180,0])
Union();